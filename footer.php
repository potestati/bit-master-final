<footer>
    <div class="col-sm-4">
        <h3>Skola koja ce Vas nauciti da radite na konkretnom radnom mestu <br> prekvalifikujte se uspesno uz nasu pomoc</h3>
        <a href="https://www.facebook.com/Business.IT.master/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="https://www.instagram.com/bit_master_ns/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a href="https://www.linkedin.com/in/bit-master-372573153/"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
        <h4>BIT MASTER</h4>
        <p>Mobilni telefon &#124;
            <a href="tel:+0692434444">
                <span style="color: #cb3438;">069 243 4444</span>
            </a>
        </p>
        <p>Stražilovska 16, Novi Sad 21000 &#124;<br> Telefon &#58;
            <a href="tel:0213829443">
                <span style="color: #cb3438;">021&#47;3829443</span> &#124;
            </a>
            Email &#58; 
            <a href="mailto:office@bitmaster.rs?Subject=Kursevi%20registracija" target="_top">
                <span style="color: #cb3438;">office&#64;bitmaster&#46;rs</span>
            </a>
        </p>
    </div>
    <div class="col-sm-4">
        <h3>Kursevi</h3>
        <ul>
            <a href="../it-dizajn/"><li>Programiranje & Front End</li></a>
            <a href="../marketing/"><li>Marketing</li></a>
            <a href="../biznis/"><li>Finansije & Računovodstvo</li></a>
            <a href="../it-dizajn/"><li>SEO</li></a>
        </ul>
    </div>
    <div class="col-sm-4">
        <h3>Prijavi se sada</h3>
        <a href="../register.php" class="btn btn-info" role="button">Online prijava</a>
        <br>
        <a href="../register.php" class="btn btn-info" role="button">Registracija</a>
    </div>
    <hr>
    <div class="socket">
        <div class="row">
            <div class="col-md-6 footer-left">
                <p>&copy; Copyright 2018. by BITmaster</p>
            </div>
            <div class="col-md-6 footer-right">
                <p>&copy; Design and Develop by <a href="#">BITmaster Digital Solution</a> | <a href="http://www.bitmaster.rs/">Web Support</a></p>
            </div>
        </div>
    </div>
</footer>
