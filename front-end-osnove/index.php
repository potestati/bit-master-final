<!DOCTYPE html>
<html class="demo-1 no-js">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="../css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
        <link href="../css/owl.carousel.css" rel="stylesheet" type="text/css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="../css/component.css" />
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="../css/responsive.css">
    </head>
    <body>
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="../home/">
                            <img src="../img/logo.png" alt="logo">
                        </a>
                    </div>
                    <?php include '../menu-main.php'; ?>
                </div>
            </div>
            <div class="category-position">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="../home/">Home</a> <span class="divider">/</span></li>
                        <li><a href="../it-dizajn/">Kategorije</a> <span class="divider">/</span></li>
                        <li class="active">Front End Osnovni Nivo</li>
                    </ul>
                </div>
            </div>
            <div class="inner-img">
                <img src="../img/single-web-design.jpg">
            </div>
        </header>

        <div class="partners">
            <div class="container">
                <div class="classroom">
                    Detaljni podaci o kursu
                </div>
                <div id="owl-demo">
                    <div class="item">
                        <p>
                            Trajanje kursa:<br>
                            <span>16 predavanja po 1h 30min.</span>                        
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Intenzitet predavanja:<br>
                            <span>2 puta nedeljno</span>                      
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Termin održavanja časova:<br>
                            <span>vikendom i večernji</span> 
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Struktura kursa<br>
                            <span>izrada websajta</span> 
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Plaćanje mesečno<br>
                            <span>1 mesec jedna rata</span>                         
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Veštine<br>
                            <span>Izrada websajta</span> 
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Konsultacije<br>
                            <span>Moguće po dogovoru</span>    
                        </p>
                    </div>
                    <div class="item">
                        <p>
                            Rezervacija kursa<br>
                            <span>1750 dinara</span>                        
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-course">
            <a class="rezervisi" href="../register.php">Rezervisi</a>
        </div>
        <div class="content">
            <h2><strong>PROMOTIVNA CENA – 7500 dinara</strong></h2>
            <br><br><br>
            <h1>Nauci da napravis Website</h1>
            <p>
                Ovo je škola u kojoj ćes da naučiš da praviš vrhunske Web Portale i Websajt-ove. 
                Udji u svet programiranja počni sa HTML5 i CSS3. 
                Nauči šta je responsive i napravi responsive sajt koji je funkcionalan na svim aparatima tabletima mobilnim telefonima.
            </p>
            <br><br><br>
            <h2>Šta radimo na kursu</h2>
            <p>
                Zajedno sa predavačem ćete praviti website , učiti na konkretnom primeru HTML5, CSS3, responsive. Uvešćemo te u svet informacionih tehnologija.
                Ukoliko želite da programirate neophodno je da jako dobro naučite i front end. Ako ne znate ništa a želite da udjete u svet programiranja ovo je odličan način da počnete.
            </p>
            <!--            <a target="_blank" class="rezervisi plan" href="../progr/Osnove php.pdf">Više informacija...vidi</a>-->
            <div class="data">
                <div class="col-sm-4">
                    <img src="../img/send.png" alt="send" class="">
                    <h4>Šta Dobijaš</h4>
                    <p>Za tebe imamo napredne kurseve Front End, gde ćes naučiti Bootstrap biblioteku , Uvešćemo te u JavaScript, JQUERY , AJAX, nauči šta je json format i kako se sa njim radi na aplikacijama.
                        Ne gubi vreme dok svet odlazi u sve veću zavisnost od Infromacionih Tehnologija , budi in budi IT - odmah!
                    </p>
                </div>
                <div class="col-sm-4">
                    <img src="../img/geometry.png" alt="send" class="">
                    <h4>Šta Dalje</h4>
                    <p>
                        Na kursu se uči izrada websajta kroz praktican rad. Kurs traje mesec dana za to vreme se izradjuju dva websajta kako bi polaznik i utvrdio stečeno znanje.
                        Ovaj kurs je namenjen svim ljudima , nije potrebno nikakvo predznanje sem znanja korisničkog rada na računaru. Tu smo da Vas sve naučimo.
                        Ne gubi vreme iskoristi priliku prijavi se - sada!
                    </p>
                </div>
                <div class="col-sm-4">
                    <img src="../img/geography.png" alt="send" class="">
                    <h4>Šta ćes naučiti</h4>
                    <p>
                        Na kursu učimo ljude da rade praktično. Za svaku oblast smo doveli predavace koji rade istovremeno u kompanijama, ljude iz struke. Oni će Vam na konkretnim primerima objasniti kako se radi.
                        Pokazace Vam proces rada i vi cete učestvovati u procesu izrade takodje. Mi teoriju predajemo kroz praksu , a ne predajemo teoriju da biste imali ideju i imaginaciju prakse.
                    </p>
                </div>
            </div>
        </div>
        <div class="termini-obuke">
            <span class="fa-stack fa-lg">
                <i class="fa fa-grav fa-5x fa-inverse"></i>
            </span>
            <div class="container">
                <div class="col-md-6">
                    <i class="fa fa-calendar fa-5x"></i>
                    <h4>Grupa I</h4>
                    <p>
                        Početak: februar 2018 godine. Nastava se pohađa radnim danima / subotom. 
                        Kontaktirati školu: 09 - 17h. Za sve dodatne informacije možete nas kontaktirati 
                        na brojeve telefona 069/ 243 44 44 i 021/ 382 94 43. Ili nam pišite na mejl office@bitmaster.rs
                    </p>
                    <a class="rezervisi termini" href="../register.php">Rezervisi</a>
                </div>
                <div class="col-md-6">
                    <i class="fa fa-calendar fa-5x"></i>
                    <h4>Grupa II</h4>
                    <p>
                        Početak: februar 2018 godine. Nastava se pohađa radnim danima / subotom. 
                        Kontaktirati školu: 09 - 17h. Za sve dodatne informacije možete nas kontaktirati 
                        na brojeve telefona 069/ 243 44 44 i 021/ 382 94 43. Ili nam pišite na mejl office@bitmaster.rs
                    </p>
                    <a class="rezervisi termini" href="../register.php">Rezervisi</a>
                </div>
            </div>
        </div>
        <?php include('../footer.php'); ?>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="../js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="../js/main.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {

                $("#owl-demo").owlCarousel({
                    autoPlay: 3000,
                    mouseDrag: true,
                    items: 4,
                    itemsDesktop: [1199, 3],
                    itemsDesktopSmall: [979, 3],
                    navigation: true,
                    navigationText: ["", ""],
                    rewindNav: true,
                    scrollPerPage: false
                });
            });
        </script>
        <!-- / Script-->
    </body>
</html>
