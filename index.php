<!DOCTYPE html>
<html class="demo-1 no-js">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
        <link rel="icon" href="img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
        <link href="css/owl.carousel.css" rel="stylesheet" type="text/css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="css/responsive.css">
    </head>
    <body>
        <?php
//        session_start();
//        echo $_SESSION["poruka"];
//        if (isset($_SESSION["poruka"])) {
//            global $poruka;
//            $poruka = $_SESSION["poruka"];
//        } else {
//            $poruka = '';
//        }
        ?>
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="index.php">
                            <img src="img/logo.png" alt="logo">
                        </a>
                    </div>
                    <!--                    <div class="info-header">
                                            <a href="https://www.facebook.com/Business.IT.master/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            <a href="https://www.instagram.com/bit_master_ns/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                            <a href="https://www.linkedin.com/in/bit-master-372573153/"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                        </div>-->
                    <div class="col-md-8 right">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><a href="./home/">Home</a></li>
                                    <li><a href="./skola/">O Nama</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kursevi <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="./biznis/">Biznis</a></li>
                                            <li><a href="./marketing/">Marketing</a></li>
                                            <li><a href="./it-dizajn/">IT&#47;DIZAJN</a></li>
                                        </ul>
                                    </li>
                                    <!--                <li><a href="#">Blog</a></li>-->
                                    <li><a href="./cenovnik/">Cenovnik</a></li>
                                    <li><a href="register.php">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="first-slider">
                <div id="carousel-example-generic" class="carousel slide carousel-fade">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active slide1">
                            <div class="row">
                                <div class="container">
                                    <div class="col-md-3 text-right">
                                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="img/developer.png">
                                    </div>
                                    <div class="col-md-9 text-left">
                                        <h3 data-animation="animated bounceInDown">Front End Developer</h3>
                                        <h4 data-animation="animated bounceInUp">Ne gubi vreme, nauči Bootstrap, JavaScript, SASS, jQuery, CSS3. Rasturi website</h4>             
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="item slide2">
                            <div class="row">
                                <div class="container">
                                    <div class="col-md-7 text-left">
                                        <h3 data-animation="animated bounceInDown">PHP Developer</h3>
                                        <h4 data-animation="animated bounceInUp">Udji u svet programiranja i napravi aplikaciju sa nama</h4>
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="img/back-end.png">
                                    </div>
                                </div></div>
                        </div>
                        <div class="item slide3">
                            <div class="row">
                                <div class="container">
                                    <div class="col-md-7 text-left">
                                        <h3 data-animation="animated bounceInDown">Web Design</h3>
                                        <h4 data-animation="animated bounceInUp">Kreiraj i prelep design, postani vrh u svetu dizajnera</h4>
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="img/education.png">
                                    </div>     
                                </div>
                            </div>
                        </div>
                        <div class="item slide4">
                            <div class="row"><div class="container">
                                    <div class="col-md-7 text-left">
                                        <h3 data-animation="animated bounceInDown">We are creative</h3>
                                        <h4 data-animation="animated bounceInUp">Get start your next awesome project</h4>
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <img style="max-width: 200px;"  data-animation="animated zoomInLeft" src="img/we-are-creative.png">
                                    </div>  
                                </div></div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <i class="fa fa-angle-left"></i><span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <i class="fa fa-angle-right"></i><span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </header>
        <!--        <div class="poruka"><?php
        //echo $poruka;
        // var_dump($_SESSION["poruka"]);
        ?></div>-->
        <div class="container home-data">
            <div class="home-info">
                <div class="col-sm-4">
                    <div id="f1_container">
                        <div id="f1_card" class="shadow">
                            <div class="front face">
                                <img src="img/misija-vizija.jpg"/>
                            </div>
                            <div class="back face center">
                                <h3>Pouči čoveka nečem boljem ako možeš.</h3>
                                <p>Postati jedan od vodećih centara za obuku i razvoj u oblastima savremenog  biznisa, menadžmenta,  marketinga i IT-ja. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div id="f1_container">
                        <div id="f1_card" class="shadow">
                            <div class="front face">
                                <img src="img/zasto-bit.jpg"/>
                            </div>
                            <div class="back face center">
                                <h3>Budite spremni i šansa će se ukazati!</h3>
                                <p>Biznis i IT MASTER (BIT MASTER) je agencija, 
                                    nastala kao plod dugogodišnjeg iskustva i analiziranja potreba tržista radne snage, 
                                    kako osnivača tako i predavača. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div id="f1_container">
                        <div id="f1_card" class="shadow">
                            <div class="front face">
                                <img src="img/koncept.jpg"/>
                            </div>
                            <div class="back face center">
                                <h3>Kursevi, obuke, treninzi i seminari u BiT MASTERU su organizovani vrlo jednostavno</h3>
                                <p>centralna figura je POLAZNIK (’Klijent je najvažnij figura u našoj igri, ko ne veruje neka proba drugačije’).</p>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="register.php">
                    <div class="col-sm-4 last">
                        <div id="f1_container">
                            <div id="f1_card" class="shadow">
                                <div class="front face">
                                    <img src="img/prijavi-se.jpg"/>
                                </div>
                                <div class="back face center">
                                    <h3>Prijavi se sada</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row thumb-row animacija-15">
            <h1 style="text-align: center; margin: 40px;">PONUDA OBUKA I KURSEVA KAO I PROGRAMA ZA PREKVALIFIKACIJU</h1>
            <div class="col-md-4">
                <div class="thumbnail shrink">
                    <a href="./marketing/" target="_blank">
                        <img id="thumb-img" src="img/home-marketing-seo.jpg" alt="marketing" style="width:100%">
                        <div class="caption">
                            <h2>MARKETING</h2>
                            <p>
                                Biti prvi više nije stvar prestiža, već potrebe. Savladajte tehnike koje će vam omoguciti bolju vidljivost na Internetu.
                                Naučite veštinu pisanja prodajnih tekstova i otvorite sebi vrata za jedno od najtraženijih zanimanja danas.
                                Smanjite ulaganja, povećajte prodaju.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="thumbnail">
                    <a href="./it-dizajn/" target="_blank">
                        <img id="thumb-img" src="img/informacione-tehn.jpg" alt="informacione tehnologije" style="width:100%">
                        <div class="caption">
                            <h2>IT&#47;DIZAJN</h2>
                            <p>
                                Završite jedan od kurseva i isprogramirajte sebi buducnost kakvu želite i zaslužujete.
                                Napravite svoj website i povećajte vidljivost svojoj firmi i proizvodima.
                                Lakše pronadjite posao u trenutno najbrže rastućoj industriji.
                            </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="thumbnail">
                    <a href="./biznis/" target="_blank">
                        <img id="thumb-img" src="img/finansije-racunovod.jpg" alt="Finansije" style="width:100%">
                        <div class="caption">
                            <h2>BIZNIS</h2>
                            <p>Steknite veštine i znanja koja ce vam omogućiti napredovanje u poslu i karijeri.
                                Unapredite svoje poslovanje i uklonite sve prepreke, neka vas biznis nema granica.
                                Povećajte šanse za dobijanje posla.</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div id="images">
            <?php
            $images = array('img/galerija/class1.jpg', 'img/galerija/class2.jpg', 'img/galerija/class3.jpg', 'img/galerija/class4.jpg', 'img/galerija/class5.jpg', 'img/galerija/class6.jpg', 'img/galerija/class7.jpg', 'img/galerija/class8.jpg');
            foreach ($images as $image) {
                ?>
                <div class="item">
                    <img src="<?php echo $image; ?>">
                </div>
            <?php }
            ?>
        </div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2808.9473373078563!2d19.84653451555027!3d45.24885767909904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b106d052bbd69%3A0xf976a84c93327dca!2sStra%C5%BEilovska+16%2C+Novi+Sad+21000!5e0!3m2!1sen!2srs!4v1510425517293" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <footer>
            <div class="col-sm-4">
                <h3>Skola koja ce Vas nauciti da radite na konkretnom radnom mestu <br> prekvalifikujte se uspesno uz nasu pomoc</h3>
                <a href="https://www.facebook.com/Business.IT.master/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="https://www.instagram.com/bit_master_ns/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="https://www.linkedin.com/in/bit-master-372573153/"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                <h4>BIT MASTER</h4>
                <p>Mobilni telefon &#124;
                    <a href="tel:+0692434444">
                        <span style="color: #cb3438;">069 243 4444</span>
                    </a>
                </p>
                <p>Stražilovska 16, Novi Sad 21000 &#124;<br> Telefon &#58;
                    <a href="tel:0213829443">
                        <span style="color: #cb3438;">021&#47;3829443</span> &#124;
                    </a>
                    Email &#58; 
                    <a href="mailto:office@bitmaster.rs?Subject=Kursevi%20registracija" target="_top">
                        <span style="color: #cb3438;">office&#64;bitmaster&#46;rs</span>
                    </a>
                </p>
            </div>
            <div class="col-sm-4">
                <h3>Kursevi</h3>
                <ul>
                    <a href="../it-dizajn/"><li>Programiranje & Front End</li></a>
                    <a href="../marketing/"><li>Marketing</li></a>
                    <a href="../biznis/"><li>Finansije & Računovodstvo</li></a>
                    <a href="../it-dizajn/"><li>SEO</li></a>
                </ul>
            </div>
            <div class="col-sm-4">
                <h3>Prijavi se sada</h3>
                <a href="../register.php" class="btn btn-info" role="button">Online prijava</a>
                <br>
                <a href="../register.php" class="btn btn-info" role="button">Registracija</a>
            </div>
            <hr>
            <div class="socket">
                <div class="row">
                    <div class="col-md-6 footer-left">
                        <p>&copy; Copyright 2018. by BITmaster</p>
                    </div>
                    <div class="col-md-6 footer-right">
                        <p>&copy; Design and Develop by <a href="#">BITmaster Digital Solution</a> | <a href="http://www.bitmaster.rs/">Web Support</a></p>
                    </div>
                </div>
            </div>
        </footer>


        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!--        <script src="js/snap.svg-min.js"></script>-->
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="js/main.js" type="text/javascript"></script>
        <!-- / Script-->
    </body>
</html>
