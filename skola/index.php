<!DOCTYPE html>
<html lang="en">
    <head>	
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="../img/favicon.png" type="image/x-icon">
        <link rel="icon" href="../img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS file -->
        <link href="../lib/bootstrap-3.0.3/css/bootstrap.min.css" rel="stylesheet" />
        <link href="../lib/bootstrap-3.0.3/css/bootstrap-theme.min.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="../blog.css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <link rel="stylesheet" type="text/css" href="../css/responsive.css">
    </head>
    <body>
        <!-- Header -->
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="../home/">
                            <img src="../img/logo.png" alt="logo">
                        </a>
                    </div>
                    <!--                    <div class="info-header">
                                            <a href="https://www.facebook.com/Business.IT.master/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            <a href="https://www.instagram.com/bit_master_ns/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                            <a href="https://www.linkedin.com/in/bit-master-372573153/"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                        </div>-->
                    <div class="col-md-8 right">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><a href="../home/">Home</a></li>
                                    <li><a href="../skola/">O Nama</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kursevi <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="../biznis/">Biznis</a></li>
                                            <li><a href="../marketing/">Marketing</a></li>
                                            <li><a href="../it-dizajn/">IT&#47;DIZAJN</a></li>
                                        </ul>
                                    </li>
                                    <!--                <li><a href="#">Blog</a></li>-->
                                    <li><a href="../cenovnik/">Cenovnik</a></li>
                                    <li><a href="../register.php">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="category-position">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="../home/">Home</a> <span class="divider">/</span></li>
                        <li><a href="../skola/">Kategorija</a> <span class="divider">/</span></li>
                        <li class="active">O Nama</li>
                    </ul>
                </div>
            </div>
        </header>
        <!-- Body -->
        <div class="container">
            <h1>O Nama</h1>				
            <div class="row about-img">
                <div class="col-md-3 col-sm-3">
                    <img class="img-thumbnail img-responsive photo" src="../img/rast.jpg" />
                </div>
                <div style="font-size: 16px;"class="col-md-9 col-sm-9">
                    <h2 style="margin-top: 0; font-size: 20px;">Budite spremni i sansa ce se ukazati!</h2>
                    <p>
                        <strong>B</strong>iznis i <strong>IT</strong> MASTER (BIT MASTER) je agencija, nastala kao plod dugogodisnjeg iskustva i analiziranja potreba trzista radne snage, kako osnivaca tako i predavaca. Kroz svakodnevni rad i razgovor sa preduzetnicima, mikro, malim i velikim pravnim licima, njihovim zaposlenima, studentima, djacima te pracenjem stalnih promena koje donosi savremeno poslovanje, kreirani su kursevi, obuke, treninzi i seminari koji mogu da odgovore njihovim zahtevima. 
                        Napredak tehnike i tehnologije, otvaranje novih trzista roba, usluga, kapitala i radne snage, dovode do promena u opisu radnih mesta i profila zaposlenih koji bi adekvatno odgovorili na sve potrebe poslodavaca. Te promene su sve brze i intenzivnije. Prirodno je da u takvoj situaciji covek zeli da raste, napreduje i da se usavrsava kako bi adekvatno reagovao na novonastalo stanje. Ovo nas vodi do dubljeg znacenja BIT MASTERA:  pomoci ljudima da u prijatnom ambijentu, koristeci savremenu opremu i uceci od kvalitetnih predavada, pronadju BIT (sustinu) u svojoj profesionalnoj karijeri. 
                    </p>

                    <p>
                        Prostorije BIT MASTERA se nalaze u Novom Sadu i Backoj Palanci, sa namerom da poslovanje prosirimo i u druge gradove. Pozivamo sve zainteresovane, koji imaju ideje, znanja i iskustva, a zele sve to da podele sa drugima, da nas kontaktiraju na mail adresu <a href="mailto:office@bitmaster.rs?Subject=Kursevi%20registracija" target="_top"><strong style="color: black;">office@bitmaster.rs</strong></a>   
                    </p>
                </div>
            </div>	

<!--            <p class="social-buttons text-center">
                <button type="button" class="btn rezervisi">Facebook</button> &nbsp;
                <button type="button" class="btn rezervisi">Twitter</button>
            </p>-->

            <br />
            <p class="lead text-center">
                Uvek se vodimo maksimom Vilijama Džejmsa: "Prava svrha života je posvetiti ga nečemu što će nas nadživeti." 
            </p>
            <div class="row">
                <div class="col-md-4">
                    <div class="thumbnail">
                        <a href="#koncept">
                            <img src="../img/cilj.jpg" class="img-responsive" />
                        </a>
                        <div class="caption text-center">
                            <h4>KONCEPT</h4>
                            <p>
                                Kursevi, obuke, treninzi i seminari u BiT MASTERU su organizovani vrlo jednostavno, centralna figura je POLAZNIK (’Klijent je najvažnij figura u našoj igri, ko ne veruje neka proba drugačije’). 
                            </p>
                            <!--                            <button class="btn btn-sm btn-success">buy now!</button>-->
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <a href="#odgovornost">
                            <img src="../img/misija.jpg" class="img-responsive" />
                        </a>
                        <div class="caption text-center">
                            <h4>DRUŠTVENA ODGOVORNOST</h4>
                            <p>
                                “Make commitment to do something more important and bigger than yourself’’
                            </p>
                            <!--                            <button class="btn btn-sm btn-success">buy now!</button>-->
                        </div>								
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <a href="#master">
                            <img src="../img/vizija.jpg" class="img-responsive" />
                        </a>
                        <div class="caption text-center">
                            <h4>BiT MASTER</h4>
                            <p id="koncept">
                                Postati jedan od vodećih centara za obuku i razvoj u oblastima savremenog  biznisa, menadžmenta,  marketinga i IT-ja.
                            </p>
                            <!--                            <button class="btn btn-sm btn-success">buy now!</button>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about container">
            <h2>KONCEPT</h2>
            <h5>Kursevi, obuke, treninzi i seminari u BiT MASTERU su organizovani vrlo jednostavno, centralna figura je POLAZNIK (’Klijent je najvažnij figura u našoj igri, ko ne veruje neka proba drugačije’).</h5> 

            <p>Svaki polaznik mora izaći sa znanjem i veštinama zbog kojih se prijavio na jedan od kurseva. To je moguće jedino uz:</p>
            <ul>
                <li>Prijatan i konforan prostor u kojem se odvijaju obuke, kursevi i treninzi.</li> 
                <li>Kvalitetnu opremu pomoću koje se bez problema i zastoja mogu obavljati i zahtevnije radnje</li>
                <li>Kvalitetnu pripremu za svaku grupu. Predavač unapred zna strukturu, nivo znanja i očekivanja polaznika te časove prilagođava njima.</li>
                <li>Kvalifikovane predavače , stručno i praktično, koji imaju prethodno bogato iskustvo u predavanjima i prenošenju znanja (’Nije znanje znanje znati, već je znanje znanje dati’).</li>
                <li>Grupe do deset polaznika</li>
                <li  id="odgovornost">Primere iz realnog života i poslovanja</li>
            </ul>
            <p>U pregovorima smo sa nekoliko firmi iz okruženja u kojima bi polaznici mogli da primene stečena znanja i veštine kroz stručnu praksu. Najbolji polaznici će imati posebne preporuke za oblasti u kojima se istaknu na zahtev trenutnih ili budućih poslodavaca.
                Takođe, postoje zainteresovani preduzetnici i pravna lica koji će omogućiti polaznicima da rade na projektima za njihove potrebe (biznis planovi, marketing kampanje, veb sajtovi i sl.).</p>
            <br>
            <br>
            <h2>DRUŠTVENA ODGOVORNOST</h2>

            <p>
                Uvek se vodimo maksimom Vilijama Džejmsa: &#34;Prava svrha života je posvetiti ga nečemu što će nas nadživeti.&#34;
            </p>

            <p>
                U skladu sa misijom i vizijom zaposleni u BIT MASTERU imaju visoko razvijenu svest o društvenoj odgovornosti. Želimo da svaki pojedinac koji prođe obuku, kurs, trening ili seminar i koji shvati ozbiljnost i važnost odluke da se menja na bolje, bude svestan da doprinosi boljem svetu i okruženju.            </p> 
            <p>Glavne odgovornosti:</p>
            <ul>
                <li style="list-style-Type: none;
                    padding: 0 0 4px 23px;
                    background: url(https://www.computerhope.com/arrow.gif) no-repeat left top;">Pristupačne cene</li>
                <li>
                    U vremenu u kojem živimo i pored potrebe za brzim i neophodnim promenama, veliki broj ljudi ostaje uskraćen za praktična znanja i veštine zbog činjenice da je životni standard veoma nizak. Pristupačnim cenama obuka, kurseva, treninga i seminara, želimo da eliminišemo ovaj negativni faktor svakom pojedincu koji želi da se usavršava i proširuje svoje znanje.                </li> 

                <li style="list-style-Type: none;
                    padding: 0 0 4px 23px;
                    background: url(https://www.computerhope.com/arrow.gif) no-repeat left top;">
                    Popusti za nezaposlene, studente i srednjoškolce
                </li>
                <li>
                    Pored pristupačnih cena dodatna pogodnost je namenjena i onima koji nemaju redovne prihode, nezaposlenima, studentima i srednjoškolcima.                
                </li>

                <li style="list-style-Type: none;
                    padding: 0 0 4px 23px;
                    background: url(https://www.computerhope.com/arrow.gif) no-repeat left top;">Humanitarne akcije</li>
                <li>
                    Agencija će se truditi da učestvuje u humanitarnim akcijama koje se odnose na poboljšanje kvaliteta života najmlađih i akcije prikupljanja sredstva za lečenje dece.                
                </li> 
                <li>
                    Veoma je važno da svaki polaznik zna da će deo prihoda od svih obuka, kurseva, treninga i seminara ići za neku od humanitarnih akcija.                
                </li>

                <li id="master">Besplatne radionice i obuke za decu</li>

                <li style="list-style-Type: none;
                    padding: 0 0 4px 23px;
                    background: url(https://www.computerhope.com/arrow.gif) no-repeat left top;">Besplatne radionice za decu. U pripremi su besplatne radionice za najmladje</li>
                <li>Obuke za najmlađe. U planu su škole stranih jezika, obuke na računarima i sl.</li>
            </ul>
            <br><br>
            <h2>BiT MASTER</h2> 

            <h3>MISIJA</h3>
            &#34;
            Pouči čoveka nečem boljem ako možeš. Ako ti to nije moguće, onda razmisli da li si dobar. Čak su i bogovi prema ovakvim ljudima milostivi. Tako su dobri da im u mnogim slučajevima pomažu da dođu do zdravlja, do bogatstva, do slave. Tu mogućnost imaš i ti. Reci, ko bi te u tome mogao sprečiti            ?&#34;
            Marko Aurelije 
            BIT MASTER će kroz <strong>obuke, kurseve, treninge i seminare</strong> pomoći što većem broju ljudi da steknu primenjiva znanja i veštine koje će im omogućiti lični razvoj i napredovanje u karijeri i životu. 
            BIT MASTER će pružati vrhunsku uslugu i neprestalno je usavršavati i prilagođavati potrebama tržišta.
            BIT MASTER će stvoriti ambijent za zaposlene koji će ih motivisati da daju svoj maksimum i svojom kreativnošću doprinesu ostvarenju zajedničkih ciljeva (’’You don't build a business, you build people, and then people build the business’’).
            <br>
            <br>
            <br>
            <h3>VIZIJA</h3>
            <p>Postati jedan od vodećih centara za obuku i razvoj u oblastima savremenog <strong>biznisa, menadžmenta, marketinga i IT-ja.</strong> </p>
            <br>
            <br>
            <br>
        </div>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2808.9473373078563!2d19.84653451555027!3d45.24885767909904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b106d052bbd69%3A0xf976a84c93327dca!2sStra%C5%BEilovska+16%2C+Novi+Sad+21000!5e0!3m2!1sen!2srs!4v1510425517293" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <?php include('../footer.php'); ?>
        <!-- Jquery and Bootstrap Script files -->
        <script src="../lib/jquery-2.0.3.min.js"></script>
        <!-- Main Script -->
        <script src="../lib/bootstrap-3.0.3/js/bootstrap.min.js"></script>
        <script src="../js/main.js" type="text/javascript"></script>
    </body>
</html>