<!DOCTYPE html>
<html class="demo-1 no-js">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="../img/favicon.png" type="image/x-icon">
        <link rel="icon" href="../img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="../css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css">
        <link href="../css/owl.carousel.css" rel="stylesheet" type="text/css">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="../css/responsive.css">
    </head>
    <body>
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="../home/">
                            <img src="../img/logo.png" alt="logo">
                        </a>
                    </div>
                    <div class="col-md-8 right">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><a href="../home/">Home</a></li>
                                    <li><a href="../skola/">O Nama</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kursevi <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="../biznis/">Biznis</a></li>
                                            <li><a href="../marketing/">Marketing</a></li>
                                            <li><a href="../it-dizajn/">IT&#47;DIZAJN</a></li>
                                        </ul>
                                    </li>
                                    <!--                <li><a href="#">Blog</a></li>-->
                                    <li><a href="../cenovnik/">Cenovnik</a></li>
                                    <li><a href="../register.php">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="category-position">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="../home/">Home</a> <span class="divider">/</span></li>
                        <li><a href="../it-dizajn/">Kategorija</a> <span class="divider">/</span></li>
                        <li class="active">Kursevi Informacione Tehnologije</li>
                    </ul>
                </div>
            </div>
        </header>
        <section class="post-content-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12">
                        <img class="inner-header"src="../img/mac.jpg" class="img-fluid" alt="mac">
                        <p>
                            PHP je mocan programski jezik za pisanje skriptova.
                            Ako dodamo i brz i pouzdan sistem za upravljanje bazama podataka mozemo napraviti CMS aplikaciju.
                        </p>
                        <div class="well ">
                            <large><h2 class="heading-course">PROGRAMIRANJE/DIZAJN - PHP & MYSQL – OSNOVNI NIVO</h2></large>
                        </div>
                        <p>
                            Mesanjem ova dva sastojka stvoricete sinergiju koja ce vam otvoriti nove vidike i uvesti vas u svet programiranja. Svet u kojem cete pozeleti da ostanete zauvek.
                            Na kursu „PHP & MySQL – osnovni nivo“  naucite izradu efikasnih interaktivnih web aplikacija.                         
                        </p>
                        <blockquote>
                            <p>Naucite programiranje na osnovu PHP proceduralnog programskog jezika i savladajte SQL programski jezik za upravljanje bazama podataka</p>
                            <footer>Kako mi prenosimo znanje?<p>Znanje koje sticete je prakticno i stice se kroz vezbe, predavanja<cite title="Source Title"> i izradu CMS aplikacije</cite></p></footer>
                            <div class="col-sm-10 col-sm-offset-2">
                                <a href="../osnove-programiranja/" class="btn rezervisi">Detaljnije o kursu</a>
                            </div>
                        </blockquote>
                        <!-- two courses -->
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="../oop-php/" target="_blank">
                                            <img src="../img/front-end-osnove.jpg" alt="objektno orjentisano programiranje">
                                            <div class="caption">
                                                <h2>Objektno Orjentisano Programiranje <br>(PHP, PDO, MVC)</h2>
                                                <h3>Zapamtite da u životu imate dve opcije: </h3>
                                                <p>
                                                    da živite i radite na ostvarenju sopstvenih snova ili da radite na ostvarenju tudjih snova. Kojoj grupi pripadate?
                                                    Najteži je početak, prvi korak. Ali, kada jednom donesete odluku i krenete u njenu realizaciju, pokrenuli ste nezaustavljivu mašinu.
                                                    BIT MASTER je zato za vas pripremio „OOP PHP programiranja – napredni nivo“. Želimo da Vam budemo podrška i partner na putu ka uspehu i kontinuiranom usavršavanju.
                                                    Naučite osnovne principe objektno orijentisanog programiranja i njegovu primenu u izradi aplikacija. 
                                                </p>
                                            </div>
                                            <strong>Vidi vise informacija o kursu...</strong>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="../front-end-osnove/" target="_blank">
                                            <img src="../img/front-end-developer.jpg" alt="web-developer">
                                            <div class="caption">
                                                <h2>Front–End – OSNOVNI NIVO</h2>
                                                <h3>„Ako šansa ne kuca, napravite vrata“</h3>
                                                <p>
                                                    Upišite kurs „Front – End Web Developer – osnovni nivo“ i otvorite sebi vrata u svet kodiranja. 
                                                    Tamo vas čeka predavač sa višegodišnjim iskustvom u radu u velikim internacionalnim kompanijama, 
                                                    sa velikim brojem projekata u portfoliju i sa velikom željom da podeli svoje znanje sa vama.
                                                    Upoznajte se sa pojmovima:
                                                <ul>
                                                    <li>Front–End</li>
                                                    <li>HTML</li>
                                                    <li>CSS</li>
                                                </ul>
                                                Napravite svoj prvi website!
                                                <br>
                                                Udite u svet Web Developera.
                                                </p>
                                            </div>
                                            <strong>Vidi vise informacija o kursu...</strong>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="thumbnail">
                                        <a href="../bootstrap/" target="_blank">
                                            <img src="../img/php-osnove.jpg" alt="web-developer">
                                            <div class="caption">
                                                <h2>FRONT–END WEB DEVELOPER – NAPREDNI NIVO </h2>
                                                <h3>„Mrzeo sam svaki minut treninga, ali sam rekao: Ne odustaj, pati sada i ostatak života živi kao šampion!“    Muhamed Ali</h3>
                                                <p>
                                                    <strong>ČESTITAMO!</strong>
                                                    Posle završenog kursa „Front – End Web Developer – osnovni nivo“ spremni ste za jos veće izazove, kvalifikovali ste se za „Front – End Web Developer – napredni nivo“!
                                                    Tamo vas čekaju JavaScript, Bootstrap, jQuery, Responsive i drugi opasni protivnici. Ukoliko ih savladate, obezbedićete sebi život vredan življenja!
                                                    Učite od šampiona u svom poslu, naučite kako da savladate sve prepreke koje vas očekuju i postanite jak igrač na tržistu rada!
                                                </p>
                                            </div>
                                            <strong>Vidi vise informacija o kursu...</strong>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="../u-pripremi.php" target="_blank">
                                            <img src="../img/oop-php.jpg" alt="web-developer">
                                            <div class="caption">
                                                <h2 class="heading-course">Kodiraj i napravi WordPress Temu od nule</h2>
                                                <h3>Kurs u pripremi</h3>
                                                <p>
                                                    Ukoliko posedujete dovoljan nivo znanja iz sveta programiranja ili ste zavrsili osnovni kurs proceduralnog i objektno orjentisanog programiranja, znate baze i tehnike upravljanja njima. Poznato Vam je kako funkconise server, AJAX, JQUERY. Znate sta je json format i radite sa javascrpt skriptnim jezikom.
                                                    Mozete da pohadjate kurs gde cete nauciti da napravite wordpress temu od nule. Na ovom kursu cete nauciti WP funkcije i nacin kako WordPress funkcionise, pravicete sami plugins za WordPress teme a i same teme koje mozete da prodajete.
                                                </p>
                                            </div>
                                            <strong>Vidi vise informacija o kursu...</strong>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <a href="http://www.napravisamsajt.com/wordpress/" target="_blank">
                                            <img src="../img/wordpress.jpg" alt="wordpress">
                                            <div class="caption">
                                                <h2 class="heading-course">Nauci WordPress</h2>
                                                <h3>Napravite web-site za jedan dan</h3>
                                                <p>
                                                    Svedoci smo da se tehnologija svakodnevno razvija. Kupci menjanju svoje navike iz dana u dan i firme koje to shvate, požele da se digitalizuju. Na taj način one prirodno ostvaruju rast poslovanja i lakše postižu svoje ciljeve. 
                                                    <br>
                                                    Osnovni kurs iz WordPress-a namenjen je početnicima koji žele da savladaju pravila za kreiranje nove poslovne adrese - internet adrese. Polaznici će, nakon završenog kursa, naučiti kako da napravite svoj sajt ili blog i naj taj način će unaprediti vidljivost koju su do sada imali. 
                                                    <br>
                                                    Skoro svi su na web-u. Onaj ko još uvek nije, nema nijedan razlog da ne promeni strategiju.
                                                </p>
                                            </div>
                                            <strong>Vidi vise informacija o kursu...</strong>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="thumbnail">
                                        <a href="../u-pripremi.php" target="_blank">
                                            <img src="../img/slide6.jpg" alt="Kursevi Programiranja u Pripremi">
                                            <div class="caption">
                                                <h2>KURSEVI U PRIPREMI</h2>
                                                <p>
                                                    AngularJS, JavaScript, JQuery
                                                    <br>
                                                    Vue.js, Angular4
                                                    <br>
                                                    Laravel Framework OOP PHP
                                                </p>
                                            </div>
                                            <strong>Vidi vise informacija o kursu...</strong>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- two courses-->
                        <blockquote>
                            <h2 class="heading-course">ISPROGRAMIRAJTE POSAO IZ SNOVA</h2>
                            <p>
                                Naša država je IT sektor prepoznala i označila kao jedan od najvažnijih i najznačajnijih za razvoj društva i domaće ekonomije. 
                                Takodje, prema zvaničnim podacima Privredne komore Srbije, IT sektor ima visoku stopu rasta ali i veliki potencijal.                        
                            </p>

                        </blockquote>

                        <div class="image-block">
                            <img style="width: 850px; height: 560px;" src="../img/inner-img1.jpg" class="Nauci Programiranje" >
                        </div>
                        <p>
                            Nije više nepoznato da su trenutno najtraženiji i najplaćeniji poslovi u IT sektoru. Danas je skoro nemoguće pronaći kvalitetnog programera bez posla.
                            Zato smo se u Bit masteru potrudili da za vas kreiramo kurseve koji će vam omogućiti da iskoristite svoje potencijale i sami isprogramirate svoj posao iz snova!

                            Da podsetimo, svaki početak je težak! Ne treba odmah očekivati visoke zarade i pozicije. Dosta ljudi padne na ovoj prepreci. Veoma je važan kontinuitet u radu i učenju, istrajnost i spremnost da se savladaju sve teskoće. 

                            Iskusni predavači sa bogatim iskustvom, prijatan ambijent i uslovi za rad kao i zelja da iz Bit mastera izadjete zadovoljni su kvaliteti koje ćemo negovati.

                            Zato vas pozivamo da vec DANAS, bez odlaganja, iskoristite PROMOTIVNE uslove i obezbedite sebi mesto u grupama koje startuju!
                        </p>
                    </div>
                    <?php include '../sidebar.php'; ?>
                </div>
            </div> <!-- /container -->
        </section>
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2808.9473373078563!2d19.84653451555027!3d45.24885767909904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b106d052bbd69%3A0xf976a84c93327dca!2sStra%C5%BEilovska+16%2C+Novi+Sad+21000!5e0!3m2!1sen!2srs!4v1510425517293" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
        <?php include('../footer.php'); ?>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="../js/snap.svg-min.js"></script>
        <script type="text/javascript" src="../js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="../js/bootstrap.min.js"></script>
        <!-- Main Script -->
        <script src="../js/main.js" type="text/javascript"></script>
        <!-- / Script-->
    </body>
</html>
