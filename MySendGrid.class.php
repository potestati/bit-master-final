<?php 
Class MySendGrid {
	private $from;
	private $subject;
	private $to;
	private $content;
	private $api_key; // API key koji se dobija od sendgrid-a
	public function __construct($api_key = '') {
		if($api_key == null) {
			throw new Exception('First argument must be a valid SendGrid API key!');
		}
		$this->api_key = $api_key;
		return $this;
	}
	public function from($email) {
		$this->from = new \SendGrid\Email(null, $email);
		return $this;
	}
	public function to($email) {
		$this->to = new \SendGrid\Email(null, $email);
		return $this;
	}
	public function subject($subject) {
		$this->subject = $subject;
		return $this;
	}
	public function content($content) {
		$this->content = new \SendGrid\Content("text/html", $content);
		return $this;
	}
	public function send() {
		$mail = new \SendGrid\Mail($this->from, $this->subject, $this->to, $this->content);
		$sg = new \SendGrid($this->api_key);
		$response = $sg->client->mail()->send()->post($mail);
		return (bool) $response;
	}
}