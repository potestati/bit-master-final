(function ($) {
    function doAnimations(elems) {
        var animEndEv = 'webkitAnimationEnd animationend';
        elems.each(function () {
            var $this = $(this),
                    $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
                $this.removeClass($animationType);
            });
        });
    }
    var $myCarousel = $('#carousel-example-generic'),
            $firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
    $myCarousel.carousel();
    doAnimations($firstAnimatingElems);
    $myCarousel.carousel('pause');
    $myCarousel.on('slide.bs.carousel', function (e) {
        var $animatingElems = $(e.relatedTarget).find("[data-animation ^= 'animated']");
        doAnimations($animatingElems);
    });
    $('#carousel-example-generic').carousel({
        interval: 3000,
        pause: "false"
    });
})(jQuery);

/*accoridan*/
/* Accordion */
jQuery(document).ready(function () {
    function close_accordion_section() {
        jQuery('.accordion .accordion-section-title').removeClass('active');
        jQuery('.accordion .accordion-section-content').slideUp(300).removeClass('open');
    }

    jQuery('.accordion-section-title').click(function (e) {
        // Grab current anchor value
        var currentAttrValue = jQuery(this).attr('href');

        if (jQuery(e.target).is('.active')) {
            close_accordion_section();
        } else {
            close_accordion_section();

            // Add active class to section title
            jQuery(this).addClass('active');
            // Open up the hidden content panel
            jQuery('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
        }

        e.preventDefault();
    });


});
/* Photo Galery */
//    $(function () {
//
//    var $container = $('#container').masonry({
//        itemSelector: '.item',
//        columnWidth: 300
//    });
//
//    // reveal initial images
//    $container.masonryImagesReveal($('#images').find('.item'));
//});
//
//$.fn.masonryImagesReveal = function ($items) {
//    var msnry = this.data('masonry');
//    var itemSelector = msnry.options.itemSelector;
//    // hide by default
//    $items.hide();
//    // append to container
//    this.append($items);
//    $items.imagesLoaded().progress(function (imgLoad, image) {
//        // get item
//        // image is imagesLoaded class, not <img>, <img> is image.img
//        var $item = $(image.img).parents(itemSelector);
//        // un-hide item
//        $item.show();
//        // masonry does its thing
//        msnry.appended($item);
//    });
//
//    return this;
//};

