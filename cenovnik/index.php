<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="bit-master">
        <meta name="keywords" content="bit-master">
        <meta name="author" content="bit-master">
        <link rel="shortcut icon" href="../img/favicon.png" type="image/x-icon">
        <link rel="icon" href="../img/favicon.png" type="image/x-icon">
        <title>Bit Master</title>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Pacifico|Shadows+Into+Light" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <!--  Main CSS-->
        <link rel="stylesheet" type="text/css" href="../css/main.css">
        <!-- Responsive CSS -->
        <link rel="stylesheet" type="text/css" href="../css/responsive.css">
        <style type="text/css">
            .bs-example{
                margin: 20px;
            }
            .well{
                background-color: transparent;
                border: transparent;
            }
        </style>
    </head>
    <body>
        <header>
            <div class="menu-header">
                <div class="container top-header">
                    <div class="col-md-4">
                        <a href="../home/">
                            <img src="../img/logo.png" alt="logo">
                        </a>
                    </div>
                    <div class="col-md-8 right">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><a href="../home/">Home</a></li>
                                    <li><a href="../skola/">O Nama</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kursevi <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="../biznis/">Biznis</a></li>
                                            <li><a href="../marketing/">Marketing</a></li>
                                            <li><a href="../it-dizajn/">IT&#47;DIZAJN</a></li>
                                        </ul>
                                    </li>
                                    <!--                <li><a href="#">Blog</a></li>-->
                                    <li><a href="../cenovnik/">Cenovnik</a></li>
                                    <li><a href="../register.php">Kontakt</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="category-position">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="../home/">Home</a> <span class="divider">/</span></li>
                        <li><a href="../cenovnik/">Kategorije</a> <span class="divider">/</span></li>
                        <li class="active">Cenovnik</li>
                    </ul>
                </div>
            </div>
            <div class="inner-img">
                <img src="../img/cenovnik-kursevi.jpg">
            </div>
        </header>
        <!--        <div class="col-lg-9 col-md-9 col-sm-12">-->
        <div class="cenovnik-content">

            <div class="bs-example">
                <table class="table">
                    <thead>
                        <tr>
                            <th>NAZIV KURSA</th>
                            <th>TRAJANJE KURSA</th>
                            <th>REDOVNA CENA*</th>
                            <th>PROMO CENA ZA PRIJAVE*</th>
                            <th>REZERVACIJA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="active">
                            <td>OSNOVE FINANSIJSKE ANALIZE</td>
                            <td>4 NEDELJE</td>
                            <td>15000</td>
                            <td>7500</td>
                            <td>1750</td>
                        </tr>
                        <tr class="success">
                            <td>FINANSIJSKA ANALIZA - NAPREDNI NIVO</td>
                            <td>4 NEDELJE</td>
                            <td>15000</td>
                            <td>7500</td>
                            <td>1750</td>
                        </tr>
                        <tr class="info">
                            <td>BIZNIS PLAN - IZRADA I IMPLEMENTACIJA</td>
                            <td>4 NEDELJE</td>
                            <td>15000</td>
                            <td>7500</td>
                            <td>1750</td>
                        </tr>
                        <tr class="warning">
                            <td>SPOLJNA TRGOVINA</td>
                            <td>4 NEDELJE</td>
                            <td>15000</td>
                            <td>7500</td>
                            <td>1750</td>
                        </tr>
                        <tr class="danger">
                            <td>STATISTIČKA OBRADA PODATAKA</td>
                            <td>8 NEDELJA</td>
                            <td>20.000/MESEČNO</td>
                            <td> - </td>
                            <td> - </td>
                        </tr>
                        <tr class="active">
                            <td>KNJIGOVODSTVO</td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                            <td> - </td>
                        </tr>
                        <tr class="success">
                            <td>VEŠTINE PRODAJE</td>
                            <td>4 NEDELJE</td>
                            <td>15000</td>
                            <td>7500</td>
                            <td>1750</td>
                        </tr>
                        <tr class="info">
                            <td>INTERNET MARKETING</td>
                            <td>4 NEDELJE</td>
                            <td>25000</td>
                            <td>U PRIPREMI</td>
                            <td> - </td>
                        </tr>
                        <tr class="warning">
                            <td>SEO OPTIMIZACIJA</td>
                            <td>4 NEDELJE</td>
                            <td>20.000/MESEČNO</td>
                            <td> - </td>
                            <td>U PRIPREMI</td>
                        </tr>
                        <tr class="active">
                            <td>COPYWRITING</td>
                            <td>4 NEDELJE</td>
                            <td>20.000</td>
                            <td>7.500</td>
                            <td>1.750</td>
                        </tr>
                        <tr class="success">
                            <td>FRONT - END WEB DEVELOPER - OSNOVNI</td>
                            <td>4 NEDELJE</td>
                            <td>18.000</td>
                            <td>7.500</td>
                            <td>1.750</td>
                        </tr>
                        <tr class="info">
                            <td>FRONT - END WEB DEVELOPER - NAPREDNI</td>
                            <td>12 NEDELJA </td>
                            <td>25.000/MESEČNO</td>
                            <td>7.500 /MESEČNO</td>
                            <td>1.750</td>
                        </tr>
                        <tr class="warning">
                            <td>OSNOVE PROGRAMIRANJE PHP i SQL</td>
                            <td>12 NEDELJA</td>
                            <td>20.000/MESEČNO</td>
                            <td>7.500 / MESEČNO</td>
                            <td>1.750</td>
                        </tr>
                        <tr class="danger">
                            <td>OOP PHP  - NAPREDNI</td>
                            <td>12 NEDELJA </td>
                            <td>25.000/MESEČNO</td>
                            <td>12.000 / MESEČNO</td>
                            <td>1.750</td>
                        </tr>
                        <tr class="active">
                            <td>GRAFIČKI DIZAJN</td>
                            <td>4 NEDELJE</td>
                            <td>20.000</td>
                            <td>U PRIPREMI</td>
                            <td> - </td>
                        </tr>
                        <tr class="success">
                            <td>WORDPRESS</td>
                            <td>4 NEDELJE</td>
                            <td>18.000</td>
                            <td>7.500</td>
                            <td>1.750</td>
                        </tr>
                        <tr class="danger">
                            <td>JAVA PROGRAMIRANJE - POČETNI NIVO</td>
                            <td> - </td>
                            <td>20.000/MESEČNO</td>
                            <td>U PRIPREMI</td>
                            <td> - </td>
                        </tr>
                        <tr class="warning">
                            <td>JAVA PROGRAMIRANJE - SREDNJI NIVO</td>
                            <td> - </td>
                            <td>20.000/MESEČNO</td>
                            <td>U PRIPREMI</td>
                            <td> - </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--        </div>-->
        <?php include '../sidebar.php'; ?>
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2808.9473373078563!2d19.84653451555027!3d45.24885767909904!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475b106d052bbd69%3A0xf976a84c93327dca!2sStra%C5%BEilovska+16%2C+Novi+Sad+21000!5e0!3m2!1sen!2srs!4v1510425517293" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <?php include('../footer.php'); ?>
            <!-- Jquery and Bootstrap Script files -->
            <script src="../lib/jquery-2.0.3.min.js"></script>
            <script src="../lib/bootstrap-3.0.3/js/bootstrap.min.js"></script>
            <script type="text/javascript" src="../js/bootstrap.min.js"></script>
            <script src="../js/main.js"></script>
        </body>
    </html>